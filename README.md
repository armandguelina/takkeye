# Takkeye

## Membres
		Armand GUELINA
		Ibrahima SOW
		

## Presentation

TAKKAYE est une application conçue pour gérer automatiquement  l’ouverture et la fermeture des volets.
Il s’agit d’une  idée conçue dans le but des “smart homes” afin de faciliter le confort des résidents et l'avènement des machines intelligentes

		GESTION AUTOMATIQUE DES VOLETS
				ROUGE : VOLET FERMÉ
				BLEU : VOLET À MOITIÉ OUVERT
				VERT : VOLET OUVERT 
		COMFORT CLIENT
		
![RED](Images/WhatsApp Image 2023-02-08 at 6.55.09 PM.jpeg)
![BLUE](Images/WhatsApp Image 2023-02-08 at 7.04.14 PM.jpeg)
![GREEN](Images/WhatsApp Image 2023-02-08 at 7.03.39 PM.jpeg)


		INTERFACE ET DESIGN AGREEABLES
			GRAPHIQUE DES DONNÉES
			INTENSITÉ DE LA LUMIÈRE
			DATES ET HEURES
			STATUS DES VOLETS
		ECONOMIE D’ENERGIE
		
![WEBPAGE](Images/Screenshot from 2023-02-08 19-25-14.png)		
		


## Instalation

1. Telecharger et installer le logiciel Arduino depuis le site officiel : https://support.arduino.cc/hc/en-us/articles/360019833020-Download-and-install-Arduino-IDE

2. Effectuer le brachement en suivant l'illustration de l'image Screenshot from 2023-02-06 19-13-03.png dans le dossier Images
		
![ILLUSTRATION](Images/Screenshot from 2023-02-06 19-13-03.png)

3. Ouvrir et exécuter le fichier project.ino dans le logiciel d'arduino

4. Installer Python3

5. Ouvrir un terminal et exécuter le fichier project.py : python3 project.py

6. Installer npm et nodejs

7. Ouvrir le ficher server.js et modifier la line : const fileName . Changer le chemin adsolue du fichier data.txt par le nouveau chemin

8. ouvrir un terminal et entrer : npm i

9. Ouvrir un terminal et exécuter le fichier server.js : node server.js

10. Ouvrir le navigateur à l'adresse affichiée dans le terminal


