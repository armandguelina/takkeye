// Import dependencies
const express = require("express");
const app = express();
const SerialPort = require("serialport");
const Readline = require("@serialport/parser-readline");
const getLastLine = require('./filetool.js').getLastLine;
const fileName = '/home/klaranouba/Documents/Armand/Klaranouba/School/University/Master2/Courses/main/Arduino/Main/test/takkeye/data.txt' //Change if needed
var dateTime = require('node-datetime');



// Host the front end
app.use(express.static("client"));

// Start the server and initialize socket.io
const server = app.listen(4000, () => console.log(`Listening at http://localhost:4000`));
const io = require("socket.io")(server);

// Initialize lightValues
const lightValues = {
    "0": { votes: 10, label: "Light", color: "" },
};


// On new client connection
io.on("connection", (socket) => {
    io.emit("update", lightValues);

    // On new vote
    socket.on("vote", (index) => {

        // Increase the vote at index
    	 if (lightValues[index]) 
    	 {
    	 	getLastLine(fileName, 0)
				.then((lastLine)=> {
					lastLineTable = lastLine.split('\t\t');
					
					//updates
					//Color and label updates
					if(Number(lastLineTable[1])< 500)
					{
						lightValues[index].color = "#FF0000";
						lightValues[index].label = lastLineTable[0] + "LOW (Shutter Closed)";
					}
					if ((Number(lastLineTable[1])> 500) && (Number(lastLineTable[1])< 800))
					{
						lightValues[index].color = "#0000FF";
						lightValues[index].label = lastLineTable[0] + "MEDIUM (Shutter Mid-Opened)";
					}
					if(Number(lastLineTable[1])> 800)
					{
						lightValues[index].color = "#00FF00";
						lightValues[index].label = lastLineTable[0] + "HIGH (Shutter Opened)";
					}
					//Intensity update
					lightValues[index].votes = lastLineTable[1];
				})
				.catch((err)=> {
					console.error(err)
				})
        }


        // Show the lightValues in the console for testing
        console.log(lightValues);

        // Tell everybody else about the new vote
        io.emit("update", lightValues);
    });
});
