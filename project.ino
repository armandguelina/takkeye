/*
 * Created by ArduinoGetStarted.com
 *
 * This example code is in the public domain
 *
 * Tutorial page: https://arduinogetstarted.com/tutorials/arduino-light-sensor
 */


const int PIN_RED   = 13;
const int PIN_GREEN = 12;
const int PIN_BLUE  = 11;

void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  pinMode(PIN_RED,   OUTPUT);
  pinMode(PIN_GREEN, OUTPUT);
  pinMode(PIN_BLUE,  OUTPUT);
}

void loop() {
  // reads the input on analog pin A0 (value between 0 and 1023)
  int analogValue = analogRead(A0);

  //Serial.print("Analog reading: ");
  Serial.println(analogValue);   // the raw analog reading

  // We'll have a few threshholds, qualitatively determined;

  
  if (analogValue <500) 
  {
   setColor(247, 120, 138);
    delay(1000);
  }
   if ((analogValue >500) && (analogValue <800) )
  {

    setColor(0, 201, 204);
    delay(1000);
  }  
  if(analogValue >800) 
  {
    setColor(52, 168, 83);
    delay(1000);
  }
  
  
  if (Serial.available() > 0) {
        String message = Serial.readString();
        int y = message.toInt();
        Serial.println(y);
        if(y==1)
          Serial.println(1);
    }

}

/** Affiche une couleur */
void setColor(int R, int G, int B) {
  analogWrite(PIN_RED,   R);
  analogWrite(PIN_GREEN, G);
  analogWrite(PIN_BLUE,  B);
}
