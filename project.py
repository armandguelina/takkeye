import serial.tools.list_ports
from datetime import datetime


#Connection to arduino port
ports = serial.tools.list_ports.comports()
serialInst = serial.Serial()

serialInst.baudrate = 9600
serialInst.port = "/dev/ttyACM0"
serialInst.open()


while True:
	#While transmitting
	if serialInst.in_waiting:
		packet = serialInst.readline() #Read line
		valY = packet.decode('utf').rstrip('\n') #remove unwanted
		#if null
		if valY == '' :
			continue
		#else
		else :
			#print value
			print(valY)
			
			#Time
			now = datetime.now()
			dt_string = now.strftime("%d/%m/%Y %H:%M:%S") 
			#Store into file
			file = open("data.txt", "a")
			strTmp = dt_string+ '\t\t' + str(valY)
			file.write(strTmp)
			file.close()
		



